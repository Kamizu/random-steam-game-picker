/**
 * Created by Kami on 28.4.2016.
 */
var userInfoUrl = "http://rsg-kaminasri.rhcloud.com/userinfo?sid=";
var gameInfoUrl = "http://rsg-kaminasri.rhcloud.com/gameinfo?uid=";

var imagePrefix = "http://cdn.akamai.steamstatic.com/steam/apps/";
var imagePostfix = "/header.jpg";

var svgWidth = 296;
var svgHeight = 150;

var svg;
var isPlaceHolderActive = false;

if(!isPlaceHolderActive){
    svg = d3.select("body").select("div.svgContainer").append("svg")
        .attr("class", "svgPlaceholder")
        .attr("align", "center")
        .attr("width", svgWidth)
        .attr("height", svgHeight);

    svg.append("rect")
        .attr("class", "rectangle")
        .attr("rx", 2)
        .attr("ry", 2)
        .attr("x", "0")
        .attr("y", "200")
        .attr("width", svgWidth)
        .attr("height", svgHeight);
		
	d3.select("body").select(".errorText")
		.attr("class", "errorText")
		.text(function(){
			return "";
		})
	
    isPlaceHolderActive = true;
}

function initSVG(){
    d3.select("body").select("div.svgContainer").append("p")
        .attr("class", "gameText")
        .attr("align", "center")
        .text(function () {
            return randomGame.name;
        });

    svg = d3.select("body").select("div.svgContainer").append("svg")
        .attr("class", "svgMain")
        .attr("align", "center")
        .attr("width", svgWidth)
        .attr("height", svgHeight);

    svg.append("image")
        .attr("class", "image")
        .attr("xlink:href", imagePrefix + randomGame.appid + imagePostfix)
        .attr("width", svgWidth)
        .attr("height", svgHeight);

    svg.append("rect")
        .attr("class", "rectangle")
        .attr("width", svgWidth)
        .attr("height", svgHeight)
        .attr("fill", "url(#image)");
}

function updateErrorText(error){
	d3.select("body").select(".errorText")
		.text(function() {
			return error;
		});
}

function updateElements(){
    svg = d3.select("body").transition();

    d3.select("body").select("div.svgContainer").select(".gameText")
        .text(function () {
            return randomGame.name;
        });
		
	updateErrorText("");

    svg.select(".image")
        .attr("xlink:href", imagePrefix + randomGame.appid + imagePostfix)

    svg.select(".rect")
        .attr("fill", "url(#image)");
}
// Show random game
function getGame(id) {
	var timer = window.setTimeout(function() {
		updateErrorText("Make sure your profile is public and your ID is valid");
	}, 2500);
	
	if(id == ''){
		updateErrorText("Insert your Steam ID or 17 digit ID");
		clearTimeout(timer);
		return false;
	}
	
	// Check if id is steam id or 17digit id (String or number)
	if(isNaN(parseFloat(id))){
		d3.json(userInfoUrl + id.toString(), function(error, userData){
			if(error || userData == undefined){
				updateErrorText("Make sure your profile is public and your ID is valid");
			} else {	
				d3.json(gameInfoUrl + userData.response.steamid, function(json) {
					randomGame = json.response.games[Math.floor((Math.random() * json.response.games.length) + 1)]; // Range [0 - games.length]
					// Init SVG. Doesn't run after first roll
					if(isPlaceHolderActive == true) {
						//First let's remove the placeholder
						d3.select("body").select("svg").remove();
						//Then let's init the new SVG and never init it again
						initSVG();
						isPlaceHolderActive = false;
					}

					// Update data if user re-rolls
					updateElements();
					
					// Change submit buttons text to reroll
					d3.select("body").select("#submitButton")
						.text("Reroll");
					
					clearTimeout(timer);
				});
			}
		});
	} else {
		d3.json(gameInfoUrl + id, function(json) {
			if(error || userData == undefined){
				updateErrorText("Make sure your profile is public and your ID is valid");
			} else {
				randomGame = json.response.games[Math.floor((Math.random() * json.response.games.length) + 1)]; // Range [0 - games.length]

				// Init SVG. Doesn't run after first roll
				if(isPlaceHolderActive == true) {
					//First let's remove the placeholder
					d3.select("body").select("svg").remove();

					//Then let's init the new SVG and never init it again
					initSVG();
					isPlaceHolderActive = false;
				}

				// Update data if user re-rolls
				updateElements();
				
				// Change submit buttons text to reroll
				d3.select("body").select("#submitButton")
					.text("Reroll");
					
				clearTimeout(timer);
			}
		});
	}
}
// Run random game
function runRandomGame(id) {
	var timer = window.setTimeout(function() {
		updateErrorText("Make sure your profile is public and your ID is valid");
	}, 2500);
	
	// Check if ID is Steam ID or 17 digit ID (String or number)
	if(isNaN(parseFloat(id))) {
		d3.json(userInfoUrl + id.toString(), function(userData){
			d3.json(gameInfoUrl + userData.response.steamid, function(json){
				run(json);
				clearTimeout(timer);
			});
		});
	} else {
		d3.json(gameInfoUrl + id, function(json){
			run(json);
			clearTimeout(timer);
		});
	}
}
// Run random game
function run(json){
	randomGame = json.response.games[Math.floor((Math.random() * json.response.games.length) + 1)]; // Range [0 - games.length]
	window.location.replace('steam://run/' + randomGame.appid);
}